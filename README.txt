This project focusses on defining an n-gram language model
on the Gutenberg data set, applying Smoothing Techniques,
Creating a grammar and spelling checker.
https://drive.google.com/file/d/0B2Mzhc7popBga2RkcWZNcjlRTGM/edit


Part B of the project is about using Naive Bayes algorithm
to detect the kind of error, given some tagged data 
in "UseData".